require 'rails_helper'

RSpec.describe Rating, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:stars) }
    it { should validate_inclusion_of(:stars).in_range(1..5) }
  end

  describe 'associations' do
    it { should belong_to(:user) } 
    it { should belong_to(:place) } 
  end
end
