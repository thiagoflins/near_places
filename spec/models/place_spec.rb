require 'rails_helper'

RSpec.describe Place, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:latitude) }
    it { should validate_presence_of(:longitude) }
  end

  describe 'associations' do
    it { should belong_to(:user) }  
  end
end
