require 'rails_helper'

RSpec.describe RegistrationsController, type: :request do
  describe 'PUT #update' do
    context 'When a valid data is passed' do 
      it 'returns a status of ok' do
        user = create(:user)
        put '/auth', params: attributes_for(:user, email: 'mesadev@test.com'), headers: auth_headers(user)

        expect(response).to have_http_status(:ok)
      end
    end

    context 'When a invalid data is passed' do
      it 'returns a status of unprocessable_entity' do
        user = create(:user)
        put '/auth', params: attributes_for(:user, email: 'mesadev.com'), headers: auth_headers(user)

        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end