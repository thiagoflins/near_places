require 'rails_helper'

RSpec.describe "Places", type: :request do
  describe "GET /index" do
    context 'When no filter is passed' do
      it 'returns the result ordered by name in alphabetical order' do
        user = create(:user)
        create(:place, name: 'Mesa Mobile Thinking', latitude: -8.0397483, longitude: -34.9224475, user_id: user.id)
        create(:place, name: 'Atacadão Maceió', latitude: -9.596140, longitude: -35.738791, user_id: user.id)

        get '/places', headers: auth_headers(user)

        expect(json_body[0][:name]).to eq 'Atacadão Maceió'
      end
    end

    context 'When coordinates are passed as parameters' do
      it 'returns the resuls ordered by closeness of the coordinates passed' do
        user = create(:user)
        create(:place, name: 'Mesa Mobile Thinking', latitude: -8.0397483, longitude: -34.9224475, user_id: user.id)
        create(:place, name: 'Atacadão Maceió', latitude: -9.596140, longitude: -35.738791, user_id: user.id)
        create(:place, name: 'Maceió Shopping', latitude: -9.6487402, longitude: -35.7155838, user_id: user.id)
        current_location = { latitude: -9.6487402, longitude: -35.7155838 }

        get '/places', params: current_location, headers: auth_headers(user)

        expect(json_body[0][:name]).to eq 'Maceió Shopping'
      end
    end
  end

  describe 'POST #create' do
    context 'When the data passed is valid' do
      it 'retuns a status of 201' do
        user = create(:user)

        post '/places', params: { place: attributes_for(:place) }, headers: auth_headers(user)

        expect(response).to have_http_status(:created)
      end

      context 'but the place is already registered' do
        it 'returns a status of 422' do
          user = create(:user)
          place = create(:place, user_id: user.id, latitude: -8.0397483, longitude: -34.9224475)

          post '/places', 
            params: { place: { name: place.name, latitude: -8.040958, longitude: -34.9204753 } }, 
            headers: auth_headers(user)

          expect(response).to have_http_status(:unprocessable_entity)
        end
      end
    end

    context 'When the data passed is not valid' do
      it 'returns a status of 422' do
        user = create(:user)

        post '/places', params: { place: attributes_for(:place, name: '') }, headers: auth_headers(user)
        
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end
