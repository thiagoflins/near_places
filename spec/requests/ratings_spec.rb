require 'rails_helper'

RSpec.describe RatingsController, type: :request do
  describe 'GET #index' do
    it 'returns a status of 200' do
      build_index_request

      expect(response).to have_http_status(:ok)
    end

    it 'returns a list of ratings' do
      build_index_request

      expect(json_body).to_not be_empty
    end

    it 'returns ratings ordered by stars in desc order' do
      build_index_request

      expect(json_body[0][:stars]).to eq(5)
    end

    it 'returns the user an place associated to them' do
      build_index_request

      expect(json_body[0][:user_name]).to eq('Thiago')
      expect(json_body[0][:place_name]).to eq('Atacadão Maceió')
    end
  end
  describe "POST #create" do
    context 'When a valid data is passed' do
      it 'returns a status of created' do
        user = create(:user)
        place = create(:place, name: 'Atacadão Maceió', latitude: -9.596140, longitude: -35.738791, user_id: user.id)

        post "/places/#{place.id}/ratings", params: { rating: attributes_for(:rating) }, headers: auth_headers(user)

        expect(response).to have_http_status(:created)
      end
    end
  end

  def build_index_request
    user = create(:user, name: 'Thiago')
    place = create(:place, name: 'Atacadão Maceió', latitude: -9.596140, longitude: -35.738791, user_id: user.id)
    create(:rating, stars: 3, place_id: place.id, user_id: user.id)
    create(:rating, stars: 5, place_id: place.id, user_id: user.id)

    get "/places/#{place.id}/ratings", headers: auth_headers(user)
  end
end
