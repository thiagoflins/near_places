require 'rails_helper'

RSpec.describe 'Session user requests' do
  describe 'POST #create' do
    context 'When the credentials passed is valid' do
      it 'returns a OK status' do  
        build_valid_login_request

        expect(response).to have_http_status(:ok)
      end

      it 'returns the user token in the header' do
        build_valid_login_request

        expect(response.headers).to include('access-token')
        expect(response.headers).to include('token-type')
        expect(response.headers).to include('client')
        expect(response.headers).to include('expiry')
        expect(response.headers).to include('uid')
      end
    end

    context 'When the credentials are not valid' do
      it 'returns a 401 status' do
        user = create(:user)

        post '/auth/sign_in', params: attributes_for(:user)

        expect(response).to have_http_status(401)
      end
    end

    def build_valid_login_request
      user = create(:user)

      post '/auth/sign_in', params: attributes_for(:user, email: user.email)
    end
  end
end