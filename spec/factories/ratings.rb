FactoryBot.define do
  factory :rating do
    comment { "Just a simple comment" }
    stars { 5 }
    user { nil }
    place { nil }
  end
end
