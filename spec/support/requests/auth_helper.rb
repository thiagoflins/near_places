module Requests
  module AuthHelper
    def auth_headers(user)
      post '/auth/sign_in', params: { email: user.email, password: user.password }
      
      headers = response.headers.with_indifferent_access
      headers.slice(:'access-token', :client, :uid, :expiry, :'token-type')
    end
  end
end