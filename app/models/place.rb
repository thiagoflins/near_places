class Place < ApplicationRecord
  belongs_to :user
  has_many :ratings, dependent: :destroy

  validates :name, presence: true
  validates :latitude, presence: true
  validates :longitude, presence: true
  validates_with Validators::PlaceAlreadyRegisteredValidator

  acts_as_mappable default_units: :meters,
                   lat_column_name: :latitude,
                   lng_column_name: :longitude

  scope :ordered_by_name, -> { order(name: :asc) }
  scope :ordered_by_coordinates, -> (coordinates) { by_distance(origin: coordinates) } 
end
