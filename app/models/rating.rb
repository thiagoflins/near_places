class Rating < ApplicationRecord
  belongs_to :user
  belongs_to :place

  validates :stars, presence: true
  validates :stars, inclusion: 1..5

  scope :ordered_by_stars, -> { order(stars: :desc) }
end
