module Validators
  class PlaceAlreadyRegisteredValidator < ActiveModel::Validator
    def validate(record)
      @record = record
      @places = Place.in_range(0..500, origin: [record.latitude, record.longitude])
      record.errors[:base] << 'Place already registered' if place_already_exists?
    end

    private

    attr_reader :places, :record

    def place_already_exists?
      places.first&.name&.parameterize == record&.name&.parameterize
    end
  end
end