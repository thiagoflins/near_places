class PlacesQuery
  def initialize(coordinates)
    @coordinates = coordinates
  end

  def all
    return Place.ordered_by_coordinates(coordinates) if coordinates.compact.present?
    
    Place.ordered_by_name
  end

  private 

  attr_reader :coordinates
end