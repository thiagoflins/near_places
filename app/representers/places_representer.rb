class PlacesRepresenter < Representable::Decorator
  include Representable::JSON::Collection

  items class: Place do
    property :id
    property :name
    property :description
    property :latitude
    property :longitude
    collection :ratings
  end
end