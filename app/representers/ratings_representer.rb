class RatingsRepresenter < Representable::Decorator
  include Representable::JSON::Collection

  items class: Rating do
    property :stars
    property :comment
    property :user_name, exec_context: :decorator
    property :place_name, exec_context: :decorator

    def user_name
      represented.user&.name || represented.user&.email
    end

    def place_name
      represented.place&.name
    end
  end
end