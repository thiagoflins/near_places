class PlacesController < ApplicationController
  def index
    places = PlacesQuery.new(coordinates_params).all
    render json: PlacesRepresenter.new(places), status: :ok
  end

  def create
    @place = current_user.places.create!(place_params) 

    render :nothing , status: :created
  end

  private

  def place_params
    params.require(:place).permit(:name, :description, :latitude, :longitude)
  end

  def coordinates_params
    [params[:latitude], params[:longitude]]
  end
end
