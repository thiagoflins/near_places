class RatingsController < ApplicationController
  def index
    @ratings = place.ratings.ordered_by_stars
    render json: RatingsRepresenter.new(@ratings), status: :ok
  end
  def create
    place.ratings.create!(rating_params.merge(user_id: current_user.id))
    render :nothing, status: :created
  end

  private

  def place
    @place = current_user.places.find(params[:place_id])
  end

  def rating_params
    params.require(:rating).permit(:comment, :stars)
  end
end
