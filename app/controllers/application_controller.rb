class ApplicationController < ActionController::API
   include DeviseTokenAuth::Concerns::SetUserByToken
   include ExceptionsHandler

   before_action :authenticate_user!
end
