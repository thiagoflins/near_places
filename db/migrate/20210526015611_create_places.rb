class CreatePlaces < ActiveRecord::Migration[6.1]
  def change
    create_table :places do |t|
      t.string :name
      t.text :description
      t.decimal :latitude, { precision: 10, scale: 6}
      t.decimal :longitude, { precision: 10, scale: 6}
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
