Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth', controllers: {
    sessions: 'sessions',
    registrations: 'registrations'
  }

  resources :places, only: %i[index create] do
    resources :ratings
  end
end
