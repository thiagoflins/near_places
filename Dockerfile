FROM ruby:2.7.3-buster

RUN apt update && apt install -y --no-install-recommends 
RUN bundle config --global frozen 1

ENV WORKDIR /home/deploy/app
RUN useradd -ms /bin/bash deploy
USER deploy

RUN mkdir -p ${WORKDIR}
WORKDIR ${WORKDIR}

COPY Gemfile Gemfile.lock ./
RUN bundle install --without development test

COPY . .

CMD ["bundle", "exec", "puma", "-C", "config/puma.rb"]