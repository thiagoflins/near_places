# README

To make the process of development easier I like to use an hybrid approach in regards the usage of docker-compose. You find below how to setup the project to use this approach:

## Setting up

First of all you should have the Ruby version below installed:

- Ruby version: **2.7.3** 

Also, if you want to use docker-compose, make sure you have both(docker, docker-compose) installed.
## dependencies

As the project is using an hybrid approach you can choose to use it with `docker-compose` or not, but if you do not use it, you must have postgresql configured locally.

to start docker-compose you should run:

```bash
$ docker-compose up -d db
``` 
## spinning up the project

**Setting env variables:**

In the project's root there is a file example called `.env.example`, Just copy it to `.env`.

```bash
$ cp -R .env.example .env
```
To make the process easier I added default values for database on `docker-compose`. So, on your `.env` you need to add the values below:
```
DB_HOST=db
DB_USERNAME=dbuser
DB_PASSWORD=dbpass
```
***ps. To use the `DB_HOST` with the value `db` you need to add it on your `/etc/hosts`***

example:
```
127.0.0.1 db
```

**Running migrations:**

```bash
$ bundle exec rails db:create
$ bundle exec rails db:migrate
```
**Running specs:**

```bash
$  bundle exec rspec
```